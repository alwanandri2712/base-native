
        <?php 
            date_default_timezone_set('Asia/Jakarta');
        
            Class Role Extends Lib {
        
                public function __construct(){
                    
                }
        
                public function indexRole($koneksi){
                    $sql_data = $koneksi->query('SELECT * FROM role WHERE is_delete=0 ORDER BY id_role DESC');
        
                    while($data = $sql_data->fetch_object()){
                        $result[] = $data;
                    }
        
                    return $result;
        
                }
        
                public function inputRole($koneksi){
                    if (isset($_POST['submit-role'])) {
                        
                        $id_beaming  = $this->post('id_beaming');
                        $is_delete = $this->post('is_delete');
                        $update_date = $this->post('update_date');
                        $update_by = $this->post('update_by');
                        $created_date = $this->post('created_date');
                        $created_by = $this->post('created_by');
                        $keterangan = $this->post('keterangan');
                        $sisa = $this->post('sisa');
                        $jam = $this->post('jam');
                        $panjang = $this->post('panjang');
                        $bom = $this->post('bom');
                        $jenis_produksi = $this->post('jenis_produksi');
                        $no_mesin = $this->post('no_mesin');
                        $nama_benang = $this->post('nama_benang');
                        $id_benang = $this->post('id_benang');
                        $utuh = $this->post('utuh');
                        $ujung = $this->post('ujung');
                        $jam_kerja = $this->post('jam_kerja');
                        $keterangan = $this->post('keterangan');
                        $created_by = $this->post('created_by');
                        $created_date = $this->post('created_date');
                        $update_by = $this->post('update_by');
                        $update_date = $this->post('update_date');
                        $is_delete = $this->post('is_delete');
                        $ciut = $this->post('ciut');
                        $cat = $this->post('cat');
                        $id_garment = $this->post('id_garment');
                        $id_printing = $this->post('id_printing');
                        $id_stok_benang = $this->post('id_stok_benang');
                        $type = $this->post('type');
                        $operator_maliwat = $this->post('operator_maliwat');
                        $kode_hasil_open = $this->post('kode_hasil_open');
                        $panjang = $this->post('panjang');
                        $hasil_ok = $this->post('hasil_ok');
                        $kain = $this->post('kain');
                        $created_by = $this->post('created_by');
                        $created_date = $this->post('created_date');
                        $update_by = $this->post('update_by');
                        $update_date = $this->post('update_date');
                        $is_delete = $this->post('is_delete');
                        $keterangan_maliwat = $this->post('keterangan_maliwat');
                        $kilogram_maliwat = $this->post('kilogram_maliwat');
                        $meter_maliwat = $this->post('meter_maliwat');
                        $ukuran_maliwat = $this->post('ukuran_maliwat');
                        $jenis_kain_maliwat = $this->post('jenis_kain_maliwat');
                        $no_mesin_maliwat = $this->post('no_mesin_maliwat');
                        $id_beaming = $this->post('id_beaming');
                        $id_maliwat = $this->post('id_maliwat');
                        $is_delete = $this->post('is_delete');
                        $id_stenter = $this->post('id_stenter');
                        $id_maliwat = $this->post('id_maliwat');
                        $id_packing = $this->post('id_packing');
                        $is_delete = $this->post('is_delete');
                        $update_date = $this->post('update_date');
                        $update_by = $this->post('update_by');
                        $created_date = $this->post('created_date');
                        $created_by = $this->post('created_by');
                        $keterang = $this->post('keterang');
                        $kondisi_printing_a = $this->post('kondisi_printing_a');
                        $kode_produksi = $this->post('kode_produksi');
                        $id_printing = $this->post('id_printing');
                        $id_maliwat = $this->post('id_maliwat');
                        $id_stenter = $this->post('id_stenter');
                        $lebar_rata_rata = $this->post('lebar_rata_rata');
                        $panjang = $this->post('panjang');
                        $suhu = $this->post('suhu');
                        $update_date = $this->post('update_date');
                        $update_by = $this->post('update_by');
                        $created_date = $this->post('created_date');
                        $created_by = $this->post('created_by');
                        $is_delete = $this->post('is_delete');
                        $is_active = $this->post('is_active');
                        $nama_role = $this->post('nama_role');
                        $id_role = $this->post('id_role');

        
                        $name = $this->post('name');
                        
                        $data = array(
                            'name'              => $name,
                            'created_by'        => $_SESSION['username'],
                            'created_date'      => date('Y-m-d H:i:s')
                        );
        
                        $result = $this->insert($koneksi,'role',$data);
        
                        if ($result === true) {
                            $this->setMessage('Yeayy!!','Berhasil Menambahkan Data','success');
                            $this->redirect('?pages=input-role'); 
                        } else {
                            $this->setMessage('Opps!!','Gagal Menambahkan Data','error');
                            $this->redirect('?pages=input-role'); 
                        }
                    } else {
                        $this->redirect('?pages=input-role'); 
                    }
                }
        
                public function updateRole($koneksi){
                    if (isset($_POST['submit-role'])) {
        
                        $name = $this-post('name');
                        
                        $data = array(
                            'name'           => $name,
                            'update_by' 	 => $_SESSION['username'],
                            'update_date' 	 => date('Y-m-d H:i:s'),
                        );
        
                        $where = array(
                            'id_role' => abs($this-post('id_role')),
                        );
                        
                        $result = $this->update($koneksi,'role',$data,$where);
        
        
                        if ($result  === true) {
                            $this->setMessage('Yeayy!!','Berhasil Update Data','success');
                            $this->redirect('?pages=role'); 
                        } else {
                            $this->setMessage('Opps!!','Gagal Update Data','error');
                            $this->redirect('?pages=role'); 
                        }
                    } else {
                        $this->redirect('?pages=role'); 
                    }
                }
        
                public function deleteRole($koneksi){
                    if (isset($_GET['id_role'])) {
                        
                        $id_role = abs($_GET['id_role']);
                        $is_delete = 1;
        
                        $data = array(
                            'is_delete' => $is_delete
                        );
        
                        $where = array(
                            'id_role' => $id_role
                        );
        
                        $result = $this->update($koneksi,'role',$data,$where);
        
                        if ($result === true) {
                            $this->setMessage('Yeayy!!','Berhasil Delete Data','info');
                            $this->redirect('?pages=role');
                        } else {
                            $this->setMessage('Opps!!','Gagal Delete Data','error');
                            $this->redirect('?pages=role');
                        }
                    } else {
                        $this->redirect('?pages=role'); 
                    }
        
                }
            }
        
            /*  COPY / CUT TO FOLDER /Routes/index.php */
        
            // case 'role':
            //     include 'App/role/v_role.php';
            //     break;
            // case 'input-role':
            //     include 'App/role/v_input_role.php';
            //     break;
            // case 'proses-input-role':
            //     include 'App/role/func.role.php';
            //     $role = new Role();
            //     $role->inputRole($koneksi);
            //     break;
            // case 'edit-role':
            //     include 'App/role/v_edit_role.php';
            //     break;
            // case 'proses-edit-role':
            //     include 'App/role/func.role.php';
            //     $role = new Role();
            //     $role->updateRole($koneksi);
            //     break;
            // case 'delete-role':
            //     include 'App/role/func.role.php';
            //     $role = new Role();
            //     $role->deleteRole($koneksi);
            //     break;
        
            /* END GENERATE FUNCTION & ROUTES */
        
        
        ?>
    
        