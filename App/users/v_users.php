<?php

if(!isset($_SESSION['username'])) {
   header('location:?login');
} else {
   $username = $_SESSION['username'];
}

?>

  <!-- Sidenav -->
  <?php include 'layout/sidebar.php'; ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include 'layout/navtop.php'; ?>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Users</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Users</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <button type="button" class="btn btn-primary float-right" onclick="window.location.href='?pages=input-users'"><i class="fas fa-plus"></i> Users</button>
              <h3 class="mb-0">Users</h3>
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>username</th>
                    <th>Role</th>
                    <th>Permission</th>
                    <th>Last Login</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach($data_users as $data) :?>
                  <tr>
                    <th><?= $no++ ?></th>
                    <td><?= $data->username;?></td>
                    <td><?= $data->nama_role;?></td>
                    <td><?= $data->permission_create == 1 ? '<button class="btn btn-sm btn-success">Create</button>' : NULL?> <?= $data->permission_read == 1 ? '<button class="btn btn-sm btn-primary">Read</button>' : NULL?><?= $data->permission_update == 1 ? '<button class="btn btn-sm btn-warning">Update</button>' : NULL?><?= $data->permission_delete == 1 ? '<button class="btn btn-sm btn-danger">Delete</button>' : NULL?></td>
                    <td><?= $data->last_login;?></td>
                    <td class="table-actions">
                      <a href="?pages=edit-users&id_user=<?=  $data->id_user ?>" class="table-action" data-toggle="tooltip" data-original-title="Edit Users">
                        <i class="fas fa-user-edit"></i>
                      </a>
                      <a data-toggle="modal" data-target="#ModalDelete<?=  $data->id_user ?>" href="#" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete Users">
                        <i class="fas fa-trash"></i>
                      </a>
                    </td>
                  </tr>
                  <!-- Modal -->
                  <div class="modal fade" id="ModalDelete<?=  $data->id_user ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Confirm Delete</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          Apakah Anda Yakin Menghapus Data <b> <?= $data->username;?></b> ?
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" onclick="window.location.href='?pages=delete-users&id_user=<?=  $data->id_user ?>'">Yakin !!!</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end modal delete -->
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <?php include 'layout/footer.php'; ?>
    </div>
  </div>
