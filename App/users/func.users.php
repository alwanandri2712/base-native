<?php

class Users extends Lib {

    public function indexUsers(){
        $data = $this->db->query("SELECT * FROM user INNER JOIN role ON role.id_role = user.id_role WHERE user.is_delete=0 ORDER BY user.id_user DESC")->rowObject();
        return $data;
    }

    public function inputUsers(){
        if (isset($_POST['submit-users'])) {
            $username  = $this->post('username');
            $cek_usrnm = $this->db->query("SELECT username FROM user WHERE username='$username'")->rowCount();
            if ($cek_usrnm == 0) {
                if ($_SESSION['permission_create'] == 1) {
                    $data = array(
                        'id_user'           =>  $this->generate_uuid(),
                        'id_role'           =>  $this->post('level'),
                        'username'          =>  $this->post('username'),
                        'password'          =>  md5($this->post('password')),
                        // 'level'             =>  $this->post('level'),
                        'permission_create' =>  $this->post('permission_create'),
                        'permission_read'   =>  $this->post('permission_read'),
                        'permission_update' =>  $this->post('permission_update'),
                        'permission_delete' =>  $this->post('permission_delete'),
                        'created_by'        =>  $this->session('username'),
                        'created_date'      =>  date("Y-m-d H:i:s"),
                        'is_delete'         => '0',
                        'is_active'         => '0' 
                    );
        
                    
                    $result = $this->insert('user',$data);

                    // var_dump($result);die;
                    // print_r($result); die();
                    if ($result) {
                        $this->setMessage('Yeayy!!','Berhasil Menambahkan Data','success');
                        $this->redirect("?pages=users"); 
                    } else {
                        $this->setMessage('Opps!!','Gagal Menambahkan Data','error');
                        $this->redirect("?pages=users"); 
                    }
                } else {
                    $this->setMessage('Opps!!','Gagal Menambahkan Data,Anda Tidak Berhak !!!','error');
                    $this->redirect("?pages=users");
                }
            } else {
                $this->setMessage('Opps!!','Data Sudah Ada !!!','error');
                $this->redirect("?pages=users");
            }
        } else {
            $this->redirect("?pages=users");
        }
    }
    
    public function updateUsers(){
        if (isset($_POST['submit-users'])) {
            if ($_SESSION['permission_update'] == 1) {
                if ($_POST['password'] != "") {
                    $data = array(
                        'username'          => $this->post('username'),
                        'password'          => md5($this->post('password')),
                        'id_role'           => $this->post('level'),
                        'permission_create' => $this->post('permission_create'),
                        'permission_read'   => $this->post('permission_read'),
                        'permission_update' => $this->post('permission_update'),
                        'permission_delete' => $this->post('permission_delete'),
                        'update_by' 	    => $this->session('username'),
                        'update_date' 	    => date("Y-m-d H:i:s"), 
                    );
        
                    $where = array(
                        'id_user' => htmlspecialchars($this->post('id_user'))
                    );

                    var_dump($data); die();
                    
                    $result = $this->update('user',$data,$where);
        
                    if ($result) {
                        $this->setMessage('Yeayy!!','Berhasil Update Data','success');
                        $this->redirect("?pages=users"); 
                    } else {
                        $this->setMessage('Opps!!','Gagal Update Data','error');
                        $this->redirect("?pages=users"); 
                    }
                } else {
                    $data = array(
                        'username'          => $this->post('username'),
                        'id_role'           => $this->post('level'),
                        'permission_create' => $this->post('permission_create'),
                        'permission_read'   => $this->post('permission_read'),
                        'permission_update' => $this->post('permission_update'),
                        'permission_delete' => $this->post('permission_delete'),
                        'update_by' 	    => $this->session('username'),
                        'update_date' 	    => date("Y-m-d H:i:s"), 
                    );
                    
                    // var_dump($data);die;

                    $where = array(
                        'id_user' => htmlspecialchars($this->post('id_user'))
                    );
                    
                    $result = $this->update('user',$data,$where);
        
                    // var_dump($result);die;
                    if ($result) {
                        $this->setMessage('Yeayy!!','Berhasil Update Data','success');
                        $this->redirect("?pages=users"); 
                    } else {
                        $this->setMessage('Opps!!','Gagal Update Data','error');
                        $this->redirect("?pages=users"); 
                    }
                }
            } else {
                $this->setMessage('Opps!!','Gagal Delete Data,Anda Tidak Berhak !!!','error');
                $this->redirect("?pages=users"); 
            }
            
        } else {
            $this->setMessage('Opps!!','Gagal Update Data','error');
            $this->redirect("?pages=users");  
        }
    }

    public function deleteUsers(){
        if (isset($_GET['id_user'])) {
            if ($_SESSION['level'] == 'admin') {
                if ($_SESSION['permission_delete'] == 1) {
                    $id_user = filter_input(INPUT_GET,'id_user',FILTER_SANITIZE_SPECIAL_CHARS);

                    $data = array(
                        'is_delete' => 1
                    );

                    $where = array(
                        'id_user' => $id_user
                    );

                    $result = $this->update('user',$data,$where);

                    if ($result) {
                        $this->setMessage('Yeayy!!','Berhasil Delete Data','info');
                        $this->redirect("?pages=users");
                    } else {
                        $this->setMessage('Opps!!','Gagal Delete Data','error');
                        $this->redirect("?pages=users");
                    }
                } else {
                    $this->setMessage('Opps!!','Gagal Delete Data,Anda Tidak Berhak !!!','error');
                    $this->redirect("?pages=users");
                }
            } else {
                $this->setMessage('Opps!!','Gagal Delete Data . Anda Bukan Admin !!!','error');
                $this->redirect("?pages=users");
            }
        } else {
            $this->setMessage('Opps!!','Gagal Delete Data','error');
            $this->redirect('?pages=users');
        }
    }
}

?>