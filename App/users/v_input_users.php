<?php

if(!isset($_SESSION['username'])) {
   header('location:?login');
} else {
   $username = $_SESSION['username'];
}
?>
  <!-- Sidenav -->
  <?php include 'layout/sidebar.php'; ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include 'layout/navtop.php'; ?>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Add Data Users</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Users</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <!-- <div class="card-header">
          <h3 class="mb-0">Form group in grid</h3>
        </div> -->
        <!-- Card body -->
        <div class="card-body">
          <!-- Form groups used in grid -->
          <div class="row">
            <div class="col-md-4">
              <form action="?pages=proses-input-users" method="POST">
                <div class="form-group">
                  <label class="form-control-label" for="example3cols1Input" style="font-weight: bold;font-size: 20">Users Info:</label>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label for="" class="form-control-label">Role <span style="color:red">*</span></label>
                <select name="level" class="form-control" id="">
                    <option readonly>-- Select Role Users --</option>
                    <?php $sql_role = $db->query("SELECT * FROM role ORDER BY id_role DESC")->rowArray() ?>
                    <?php foreach ($sql_role as $data_role): ?>
                      <option value="<?= $data_role['id_role']?>"> <?= $data_role['nama_role']?></option>
                    <?php endforeach ?>
                </select>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <label for="" class="form-control-label">Username <span style="color:red">*</span></label>
                <input type="text" class="form-control" name="username" placeholder="username" required>
              </div>
              <div class="col-md-6">
                <label for="" class="form-control-label">Password <span style="color:red">*</span></label>
                <input type="text" class="form-control" name="password" placeholder="Password" requored>
              </div>
              <div class="col-md-6">
                <label for="" class="form-control-label">Permission</label>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="permission_create" value="1">
                        </div>
                    </div>
                    <input type="text" class="form-control" placeholder="Create" readonly>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="permission_read" value="1">
                        </div>
                    </div>
                    <input type="text" class="form-control" placeholder="Read" readonly>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="permission_update" value="1">
                        </div>
                    </div>
                    <input type="text" class="form-control" placeholder="Update" readonly>
                </div>
                <div class="input-group mb-3">
                    <div class="input-group-prepend">
                        <div class="input-group-text">
                            <input type="checkbox" name="permission_delete" value="1">
                        </div>
                    </div>
                    <input type="text" class="form-control" placeholder="Delete" readonly>
                </div>
              </div>
            </div>
            
            <br>
              <button type="submit" name="submit-users" class="btn btn-primary">Submit</button>
              <button type="button" class="btn btn-secondary" onclick="window.location.href='?pages=users'">Cancel</button>
            </form>
          </div>
        </div>
        <?php include 'layout/footer.php'; ?>
      </div>
    </div>
  </div>
