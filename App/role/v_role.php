<?php
if(!isset($_SESSION['username'])) {
   header('location:?login');
} else {
   $username = $_SESSION['username'];
}
?>

  <!-- Sidenav -->
  <?php include 'layout/sidebar.php'; ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include 'layout/navtop.php'; ?>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Role</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Role</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <!-- Table -->
      <div class="row">
        <div class="col">
          <div class="card">
            <!-- Card header -->
            <div class="card-header">
              <button type="button" class="btn btn-primary float-right" onclick="window.location.href='?pages=input-role'"><i class="fas fa-plus"></i> Role</button>
              <h3 class="mb-0">Role</h3>
            </div>
            <div class="table-responsive py-4">
              <table class="table table-flush" id="datatable-basic">
                <thead class="thead-light">
                  <tr>
                    <th>No</th>
                    <th>nama role</th>
                    <th>is active</th>
                    <th>Action</th>
                  </tr>
                </thead>
                <tbody>
                  <?php
                  $no = 1;
                  foreach($data_role as $data) :?>
                  <tr>
                    <th><?= $no++ ?></th>
                    <td><?= $data->nama_role;?></td>
                    <td><?= $role->getStatus($data->is_active);?></td>
                    <td class="table-actions">
                      <a href="?pages=edit-role&id_role=<?=  $data->id_role ?>" class="table-action" data-toggle="tooltip" data-original-title="Edit role">
                        <i class="fas fa-user-edit"></i>
                      </a>
                      <a data-toggle="modal" data-target="#ModalDelete<?=  $data->id_role ?>" href="#" class="table-action table-action-delete" data-toggle="tooltip" data-original-title="Delete role">
                        <i class="fas fa-trash"></i>
                      </a>
                    </td>
                  </tr>
                  <!-- Modal -->
                  <div class="modal fade" id="ModalDelete<?=  $data->id_role ?>" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog modal-dialog-centered" role="document">
                      <div class="modal-content">
                        <div class="modal-header">
                          <h5 class="modal-title" id="exampleModalLabel">Confirm Delete</h5>
                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                          </button>
                        </div>
                        <div class="modal-body">
                          Apakah Anda Yakin Menghapus Data <b> <?= $data->nama_role;?></b> ?
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button type="button" class="btn btn-primary" onclick="window.location.href='?pages=delete-role&id_role=<?=  $data->id_role ?>'">Yakin !!!</button>
                        </div>
                      </div>
                    </div>
                  </div>
                  <!-- end modal delete -->
                  <?php endforeach ?>
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
      <!-- Footer -->
      <?php include 'layout/footer.php'; ?>
    </div>
  </div>
