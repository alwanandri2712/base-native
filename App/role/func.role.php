<?php

class Role Extends Lib {

    public function indexRole(){
        $data = $this->db->query("SELECT * FROM role WHERE is_delete=0 ORDER BY id_role DESC")->rowObject();
		return $data;
    }

    public function editRole($id){
        $data = $this->db->query("SELECT * FROM role WHERE id_role='$id'")->result();
        return $data;   
    }

    public function inputRole(){
        if (isset($_POST['submit-role'])) {
            $name_role = $this->post('name_role');
            $cek_role  = $this->db->query("SELECT nama_role FROM role WHERE nama_role='$name_role'")->rowCount();
            if ($cek_role == 0) {
                $name_role = $this->post('name_role');
                $is_active = $this->post('is_active');
    
                $data = array(
                    'id_role'       => $this->generate_uuid(),
                    'nama_role'     => $name_role,
                    'is_active'     => $is_active,
                    'is_delete'     => '0',
                    'created_by'    => $this->session('username'),
                    'created_date'  => date("Y-m-d H:i:s")
                );

    
                $result = $this->insert('role',$data);
                // var_dump($result); die();
    
                if ($result) {
                    $this->setMessage('Yeayy!!','Berhasil Menambahkan Data','success');
                    $this->redirect("?pages=role"); 
                } else {
                    $this->setMessage('Opps!!','Gagal Menambahkan Data','error');
                    $this->redirect("?pages=role"); 
                }
            } else {
                $this->setMessage('Opps!!','Data Sudah Ada !!!','error');
                $this->redirect("?pages=input-role");
            }
        } else {
            $this->redirect("?pages=role"); 
        }
    }


    public function updateRole(){
        if (isset($_POST['submit-role'])) {
            
            $name_role = $this->post('name_role');
            $is_active = $this->post('is_active');
            
            $data = array(
                'nama_role'     => $name_role,
                'is_active'     => $is_active,
                'created_by'    => $_SESSION['username'],
                'created_date'  => date("Y-m-d H:i:s")
            );

            $where = array(
                'id_role' => htmlspecialchars($this->post('id_role')),
            );

            $result = $this->update('role',$data,$where);

            if ($result) {
                $this->setMessage('Yeayy!!','Berhasil Update Data','success');
                $this->redirect("?pages=role"); 
            } else {
                $this->setMessage('Opps!!','Gagal Update Data','error');
                $this->redirect("?pages=role"); 
            }
        } else {
            $this->redirect("?pages=role"); 
        }
    }

    public function deleteRole(){
        if (isset($_GET['id_role'])) {
            
            $id_role = filter_input(INPUT_GET,'id_role',FILTER_SANITIZE_SPECIAL_CHARS);

            // var_dump($id_role); die();
            $data = array(
                'is_delete' => 1,
            );
            $where = array(
                'id_role' => $id_role
            );

            $result = $this->update('role',$data,$where);

            if ($result == true) {
                $this->setMessage('Yeayy!!','Berhasil Delete Data','info');
                $this->redirect("?pages=role");
            } else {
                $this->setMessage('Opps!!','Gagal Delete Data','error');
                $this->redirect("?pages=role");
            }
        } else {
            $this->redirect("?pages=role"); 
        }

    }

    public function getStatus($status){
        switch ($status) {
            case '1':
                return "<button class='btn btn-success btn-sm'>Actived</button>";
                break;
            case '0':
                return "<button class='btn btn-danger btn-sm'>Inactived</button>";
                break;
            default:
                return "tidak aktived";
                break;
        }
    }
}
?>
