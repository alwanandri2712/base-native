<?php

class Login extends Lib {

    public function prosesLogin(){
        if(isset($_POST['submit'])) {
            $username = addslashes($this->post('username'));
            $pass = md5($this->post('password'));
            $tanggal_sekarang = date("Y-m-d H:i:s");
            $tgllogin = "UPDATE user set last_login='$tanggal_sekarang' where username = '$username'";
            $sql = "SELECT user.*,DATE_FORMAT(last_login,'%d %M %Y') as lastlogin FROM user WHERE username = '$username'";
            $query = $this->db->queryLogin($sql);
            $hasil = $query->fetch_assoc();
            if($query->num_rows == false) {
                $this->setMessage('Opps!!','Akun Tidak Terdaftar!','error');
                $this->redirect('?login');
            } else {
                if ($pass <> $hasil['password']) {
                    $this->setMessage('Opps!!','Password salah,Silahkan Cek Kembali !','error');
                    $this->redirect('?login');
                } else {
                    $result = $this->db->query($tgllogin);
                    $_SESSION['username']          = $hasil['username'];
                    $_SESSION['id_user']           = $hasil['id_user'];
                    $_SESSION['level']             = $hasil['level'];
                    $_SESSION['permission_create'] = $hasil['permission_create'];
                    $_SESSION['permission_read']   = $hasil['permission_read'];
                    $_SESSION['permission_update'] = $hasil['permission_update'];
                    $_SESSION['permission_delete'] = $hasil['permission_delete'];
                    $_SESSION['last_login']        = $hasil['lastlogin'];
                    $_SESSION['succes_login']      = 1;
                    $this->setMessage('Yeeay!', 'Anda Berhasil Login,Selamat datang Kembali ' . $_SESSION['username'] . ' !!', 'success');
                    $this->redirect('?pages=index');
                }
            }
        }
    }

}

?>