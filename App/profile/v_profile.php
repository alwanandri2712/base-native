<?php

if(!isset($_SESSION['username'])) {
   header('location:?login');
} else {
   $username = $_SESSION['username'];
}
?>
  <!-- Sidenav -->
  <?php include 'layout/sidebar.php'; ?>
  <!-- Main content -->
  <div class="main-content" id="panel">
    <!-- Topnav -->
    <?php include 'layout/navtop.php'; ?>
    <!-- Header -->
    <!-- Header -->
    <div class="header bg-primary pb-6">
      <div class="container-fluid">
        <div class="header-body">
          <div class="row align-items-center py-4">
            <div class="col-lg-6 col-7">
              <h6 class="h2 text-white d-inline-block mb-0">Add Data Profile</h6>
              <nav aria-label="breadcrumb" class="d-none d-md-inline-block ml-md-4">
                <ol class="breadcrumb breadcrumb-links breadcrumb-dark">
                  <li class="breadcrumb-item"><a href="#"><i class="fas fa-home"></i></a></li>
                  <li class="breadcrumb-item"><a href="#">Profile</a></li>
                </ol>
              </nav>
            </div>
          </div>
        </div>
      </div>
    </div>
    <!-- Page content -->
    <div class="container-fluid mt--6">
      <div class="card mb-4">
        <!-- Card header -->
        <!-- <div class="card-header">
          <h3 class="mb-0">Form group in grid</h3>
        </div> -->
        <!-- Card body -->
        <div class="card-body">
          <!-- Form groups used in grid -->
          <div class="row">
            <div class="col-md-4">
              <form action="?pages=proses-update-profile" method="POST">
                <div class="form-group">
                  <label class="form-control-label" for="example3cols1Input" style="font-weight: bold;font-size: 20">Profile Info:</label>
                </div>
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-6">
                <label for="" class="form-control-label">Username</label>
                <input type="text" class="form-control" name="username_profile" placeholder="Nama Role" value="<?= $_SESSION['username'] ?>" readonly>
              </div>
              <div class="col-md-6">
                <label for="" class="form-control-label">Password</label>
                <input type="text" class="form-control" name="password_profile" placeholder="New Password" >
              </div>
            </div>
            <br>
              <input type="hidden" name="id_profile" value="<?= htmlspecialchars($_SESSION['id_user']) ?>">
              <button type="submit" name="update-profile" class="btn btn-primary">Submit</button>
              <!-- <button type="button" class="btn btn-secondary" onclick="window.location.href='?pages=profile'">Cancel</button> -->
            </form>
          </div>
        </div>
        <?php include 'layout/footer.php'; ?>
      </div>
    </div>
  </div>
