<?php
if (isset($_GET['pages'])) {
    $page = $_GET['pages'];
        switch($page){
        case 'index':
            include 'App/dashboard/index.php';
            break;
        case 'users':
            include 'App/users/func.users.php';
            $users = new Users();
            $data_users = $users->indexUsers();
            include 'App/users/v_users.php';
            break;
        case 'input-users':
            include 'App/users/v_input_users.php';
            break;
        case 'proses-input-users':
            include 'App/users/func.users.php';
            $users = new Users();
            $users->inputUsers();
            break;
        case 'edit-users':
            include 'App/users/v_edit_users.php';
            break;
        case 'proses-edit-users':
            include 'App/users/func.users.php';
            $users = new Users();
            $users->updateUsers();
            break;
        case 'delete-users':
            include 'App/users/func.users.php';
            $users = new Users();
            $users->deleteUsers();
            break;
        case 'role':
            include 'App/role/func.role.php';
            $role = new Role();
            $data_role = $role->indexRole();
            include 'App/role/v_role.php';
            break;
        case 'input-role':
            include 'App/role/v_input_role.php';
            break;
        case 'proses-input-role':
            include 'App/role/func.role.php';
            $role = new Role();
            $role->inputRole();
            break;
        case 'edit-role':
            include 'App/role/func.role.php';
            $roles = new Role();
            // $mmk = $role->editRole();
            include 'App/role/v_edit_role.php';
            break;
        case 'proses-edit-role':
            include 'App/role/func.role.php';
            $role = new Role();
            $role->updateRole();
            break;
        case 'delete-role':
            include 'App/role/func.role.php';
            $role = new Role();
            $role->deleteRole();
            break;
        case 'profile':
            include 'App/profile/v_profile.php';
            break;
        case 'proses-update-profile':
            include 'App/profile/func.profile.php';
            $profile = new Profile();
            $profile->updateProfile();
            break;
        /* Whatshapp 1 */
        case 'wa1':
            include 'App/whatsapp/v_whatsapp_1.php';
            break;
        case 'wa2':
            include 'App/whatsapp/v_whatsapp_1.php';
            break;
        /* Data Dasabah */
        case 'nasabah-aktif':
            include 'App/data-nasabah/v_nasabah_aktif.php';
            break;
        case 'nasabah-tidak-aktif':
            include 'App/data-nasabah/v_nasabah_tidak_aktif.php';
            break;
        case 'logout':
        	include 'logout.php';
        	break;
        default:
            include '404.php';
            break;
        }
        
} elseif (isset($_GET['login'])) {
    $login = $_GET['login'];
        switch ($login) {
        case 'proses':
            include 'App/login/func.login.php';
            $login = new Login();
            $login->prosesLogin();
            break;
        default:
            include 'App/login/index.php';
            break;
        }
} else {
    include 'App/login/index.php';
}
?>
