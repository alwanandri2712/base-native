<?php
date_default_timezone_set('Asia/Jakarta');

class Name Extends Lib {

    public function __construct(){
        
    }

    public function indexName($koneksi){
        $sql_data = $koneksi->query("SELECT * FROM youtable WHERE is_delete=0 ORDER BY id_table DESC");

		while($data = $sql_data->fetch_object()){
			$result[] = $data;
		}
		return $result;
    }
    
    public function inputName($koneksi){
        if (isset($_POST['submit-name'])) {

            $name = $this-post('name');
            
            $data = array(
                'name' => $name,
                'created_by'        => $_SESSION['username'],
                'created_date'      => date("Y-m-d H:i:s")
            );

            $result = $this->insert($koneksi,'table',$data);

            if ($result === true) {
                $this->setMessage('Yeayy!!','Berhasil Menambahkan Data','success');
                $this->redirect("?pages=yourpage"); 
            } else {
                $this->setMessage('Opps!!','Gagal Menambahkan Data','error');
                $this->redirect("?pages=yourpage"); 
            }
        } else {
            $this->redirect("?pages=yourpage"); 
        }
    }

    public function updateName($koneksi){
        if (isset($_POST['submit-name'])) {

            $name = $this-post('name');
            
            $data = array(
                'name' => $name,
                'update_by' 	 => $_SESSION['username'],
				'update_date' 	 => date("Y-m-d H:i:s"),
            );

            $where = array(
                'id_name' => abs($_POST['id_name']),
            );
            
            $result = $this->update($koneksi,'table',$data,$where);


            if ($result  === true) {
                $this->setMessage('Yeayy!!','Berhasil Update Data','success');
                $this->redirect("?pages=yourpage"); 
            } else {
                $this->setMessage('Opps!!','Gagal Update Data','error');
                $this->redirect("?pages=yourpage"); 
            }
        } else {
            $this->redirect("?pages=yourpage"); 
        }
    }

    public function deleteName($koneksi){
        if (isset($_GET['id_name'])) {
            
            $id_name = abs($_GET['id_name']);
            $is_delete = 1;

            $data = array(
                'is_delete' => $is_delete
            );

            $where = array(
                'id_name' => $id_name
            );

            $result = $this->update($koneksi,'table',$data,$where);

            if ($result === true) {
                $this->setMessage('Yeayy!!','Berhasil Delete Data','info');
                $this->redirect("?pages=yourpage");
            } else {
                $this->setMessage('Opps!!','Gagal Delete Data','error');
                $this->redirect("?pages=yourpage");
            }
        } else {
            $this->redirect("?pages=yourpage"); 
        }
    }
}

/*  COPY TO FOLDER /Routes/index.php */

// case 'Name':
//     include 'App/Name/v_Name.php';
//     break;
// case 'input-Name':
//     include 'App/Name/v_input_Name.php';
//     break;
// case 'proses-input-Name':
//     include 'App/Name/func.Name.php';
//     $Name = new Name();
//     $Name->inputName($koneksi);
//     break;
// case 'edit-Name':
//     include 'App/Name/v_edit_Name.php';
//     break;
// case 'proses-edit-Name':
//     include 'App/Name/func.Name.php';
//     $Name = new Name();
//     $Name->updateName($koneksi);
//     break;
// case 'delete-Name':
//     include 'App/Name/func.Name.php';
//     $Name = new Name();
//     $Name->deleteName($koneksi);
//     break;

?>
