<?php

date_default_timezone_set('Asia/Jakarta');

class Lib {

  public function __construct(){
    $this->db = new Database();

  }

  public function session($data){
    return $_SESSION[$data];
  }
  
  public function set_session(){

  }
  
  public function ses_destroy(){
    
  }
  
  public function post($data){
    return $_POST[$data];
  }

  public function redirect($route){
    return header('Location:'.$route);
  }
  
  public static function setMessage($action,$message,$type){
    $_SESSION['message'] = [
      'action'  => $action,
      'message' => $message,
      'type'    => $type
    ];
  }

  public static function message(){
    if (isset($_SESSION['message'])) {
      print "
        <script>
          swal('".$_SESSION['message']['action']."', '".$_SESSION['message']['message']."', '".$_SESSION['message']['type']."')
        </script>";
      unset($_SESSION['message']);
    }
  }

  public function view($view, $data = [])
    {
        require_once 'App/' . $view . '.php';
    }


  public function insert($table,$data){
      $fields = "(";
      $values = "(";
      $index  = 0;
      
      foreach ($data as $key => $val) {
        $fieldname = ($index < count($data)-1) ? $key.", " : $key. ")";
        $valuedata = ($index < count($data)-1) ? "'".$val."', "  : "'".$val."')";

        $fields .= $fieldname;
        $values .= $valuedata;

        $index++;
      }

    $query = $this->db->query("INSERT INTO ".$table." ".$fields." VALUES ".$values." ");
    return $query;
  }

  public function update($table_name, $fields, $where) {  
    $query = '';  
    $condition = '';  
    foreach($fields as $key => $value){  
        $query .= $key . "='".$value."', ";  
    }  
    $query = substr($query, 0, -2);  
    foreach($where as $key => $value){  
        $condition .= $key . "='".$value."' AND ";  
    }  
    $condition = substr($condition, 0, -5);  

    $query = $this->db->query("UPDATE ".$table_name." SET ".$query." WHERE ".$condition."");  
    return $query;
  } 
  
  public function getTable($conn,$table_name){
    $array = array();  
    $sql_data = $conn->query("SELECT * FROM ".$table_name."");

    while($data = $sql_data->fetch_object()){
        $array[] = $data;
    }

    return $array;
  }

  public function generate_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),
      mt_rand( 0, 0xffff ),
      mt_rand( 0, 0x0fff ) | 0x4000,
      mt_rand( 0, 0x3fff ) | 0x8000,
      mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
  }
  
}



?>