<nav class="sidenav navbar navbar-vertical fixed-left navbar-expand-xs navbar-light bg-white" id="sidenav-main">
    <div class="scrollbar-inner">
      <!-- Brand -->
      <div class="sidenav-header d-flex align-items-center">
        <a class="navbar-brand" href="pages/dashboards/dashboard.html">
          <img src="assets/img/theme/angular.jpg" class="navbar-brand-img" alt="...">
          <!-- <h2>NATIVE BASE</h2> -->
        </a>
        <div class="ml-auto">
          <!-- Sidenav toggler -->
          <div class="sidenav-toggler d-none d-xl-block" data-action="sidenav-unpin" data-target="#sidenav-main">
            <div class="sidenav-toggler-inner">
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
              <i class="sidenav-toggler-line"></i>
            </div>
          </div>
        </div>
      </div>
      <div class="navbar-inner">
        <!-- Collapse -->
        <div class="collapse navbar-collapse" id="sidenav-collapse-main">
          <!-- Nav items -->
          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="?pages=index">
                <i class="fa fa-home"></i>
                <span class="nav-link-text">Dashboard</span>
              </a>
            </li>
            <?php if ($_SESSION['level'] == 'admin'):?>
              <li class="nav-item">
                <a class="nav-link collapsed" href="#navbar-maps" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="navbar-maps">
                  <i class="ni ni-map-big text-primary"></i>
                  <span class="nav-link-text">Super Admin</span>
                </a>
                <div class="collapse" id="navbar-maps" style="">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a href="?pages=users" class="nav-link">
                        <span class="sidenav-normal"> Users </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="?pages=role" class="nav-link">
                        <span class="sidenav-normal"> Role </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link collapsed" href="#whastapp" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="whastapp">
                  <i class="ni ni-send text-primary"></i>
                  <span class="nav-link-text">Whastapp Inframe</span>
                </a>
                <div class="collapse" id="whastapp" style="">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a href="?pages=wa1" class="nav-link">
                        <span class="sidenav-normal"> Whatshapp 1 </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="?pages=wa2" class="nav-link">
                        <span class="sidenav-normal"> Whatshapp 2 </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
              <li class="nav-item">
                <a class="nav-link collapsed" href="#data-nasabah" data-toggle="collapse" role="button" aria-expanded="false" aria-controls="data-nasabah">
                  <i class="ni ni-send text-primary"></i>
                  <span class="nav-link-text">Data Nasabah</span>
                </a>
                <div class="collapse" id="data-nasabah" style="">
                  <ul class="nav nav-sm flex-column">
                    <li class="nav-item">
                      <a href="?pages=nasabah-aktif" class="nav-link">
                        <span class="sidenav-normal"> Nasabah Aktif </span>
                      </a>
                    </li>
                    <li class="nav-item">
                      <a href="?pages=nasabah-tidak-aktif" class="nav-link">
                        <span class="sidenav-normal"> Nasabah Tidak Aktif </span>
                      </a>
                    </li>
                  </ul>
                </div>
              </li>
            <?php endif ?>
            <li class="nav-item">
              <a class="nav-link" href="?pages=profile">
                <i class="fa fa-user"></i>
                <span class="nav-link-text">Profile</span>
              </a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="?pages=logout">
                <i class="fa fa-power-off text-red"></i>
                <span class="nav-link-text">Logout</span>
              </a>
            </li>
          </ul>
          <!-- Divider -->
          <hr class="my-3">

          <img src="assets/img/theme/angular.jpg">
          <!-- <h1 align="center">Base Native</h1> -->
          <center>&copy; 2020 </center>
      </div>
    </div>
  </nav>
